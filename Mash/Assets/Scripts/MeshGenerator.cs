﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(MeshFilter), typeof(MeshRenderer))]
public class MeshGenerator : MonoBehaviour
{
    [SerializeField] private float _width = 10f;
    [SerializeField] private float _step = 0.1f;
    [SerializeField] private Material _material = null;
    private MeshFilter _meshFilter = null;
    private MeshRenderer _meshRenderer = null;

    private void Awake()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _meshRenderer = GetComponent<MeshRenderer>();
        Generate();
    }

    private void Generate()
    {
        List<Vector3> vertices = new List<Vector3>();
        Vector3 startPoint = new Vector3(-_width / 2f, 0f, -_width / 2f);
        int pointCount = Mathf.RoundToInt(_width / _step) + 1;

        for (int i =0; i < pointCount; i ++)
        {
            startPoint.z = -_width/2f + _step * i;

            for(int j = 0; j < pointCount; j++ )
            {
                startPoint.x = -_width/2f + _step * j;
                vertices.Add(startPoint);

              

            }
        }
        List<int> triangles = new List<int>();

        for (int i = 0; i< vertices.Count - pointCount; i++)
        {
            if((i + 1) % pointCount == 0)
            {
                continue;
            }

            triangles.Add(i);
            triangles.Add(i + pointCount);
            triangles.Add(i + pointCount + 1);

            triangles.Add(i);
            triangles.Add(i + pointCount + 1);
            triangles.Add(i + 1);
        }

        Mesh mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles, 0);
        mesh.RecalculateNormals();

        _meshFilter.mesh = mesh;
        _meshRenderer.material = _material;
    }

    
}
