﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData 
{
    private Sprite _sprite = null;
    private int _amout = 0;
    

    public ItemData(Sprite sprite, int amount)
    {
        _sprite = sprite;
        _amout = amount;
    }
   
    public Sprite ItemSprite
    {
        get
        {
            return _sprite;
        }
        set
        {
            _sprite = value;
        }
    }
    public int Amount
    {
        get
        {
            return _amout;
        }
        set
        {
            if (value < 0)
            {
                _amout = 0;
            }
            else
            {
                _amout = value;
            }
        }
    }
    
}
