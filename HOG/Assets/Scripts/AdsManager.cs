﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    [SerializeField] private string _androidKey = "85460dcd";
    public void Initialize()
    {
        IronSource.Agent.init(_androidKey);
        IronSource.Agent.validateIntegration();
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
        IronSource.Agent.showInterstitial();
        IronSource.Agent.showRewardedVideo();
        IronSource.Agent.shouldTrackNetworkState(true);

    }
    private void OnEnable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent += OnInterestitialLoaded;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += OnLoadInterestitialFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent += OnLoadInterestitiaShowSuccess;
        IronSourceEvents.onInterstitialAdClosedEvent += OnInterstitialClosed;
        
        IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

    }
    private void OnDisable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent -= OnInterestitialLoaded;
        IronSourceEvents.onInterstitialAdLoadFailedEvent -= OnLoadInterestitialFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent -= OnLoadInterestitiaShowSuccess;
        IronSourceEvents.onInterstitialAdClosedEvent -= OnInterstitialClosed;

        IronSourceEvents.onRewardedVideoAdClickedEvent -= RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent -= RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent -= RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent -= RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoAdShowFailedEvent;
    }
    private void OnInterstitialClosed()
    {
        IronSource.Agent.loadInterstitial();
    }
    private bool _isInterstitialReady = false;
    private void OnInterestitialLoaded()
    {
        _isInterstitialReady = true;
    }
    private void OnLoadInterestitialFailed(IronSourceError error)
    {
        Debug.LogError($"Interstitial load failed! Reason - {error.getDescription()}");
    }
    private void OnLoadInterestitiaShowSuccess()
    {
        _isInterstitialReady = false;
    }
    public void ShowInterstitial()
    {
        if(_isInterstitialReady)
        {
            IronSource.Agent.showInterstitial();
        }
    }

    void RewardedVideoAdStartedEvent()
    {
        IronSource.Agent.showRewardedVideo();
    }
    void RewardedVideoAdEndedEvent()
    {
        
    }
    void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
    {
    }
    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        Debug.LogError($"load failed! Reason - {error.getDescription()}");
    }
    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
    }
    void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        
        bool rewardedVideoAvailability = available;
    }
}

