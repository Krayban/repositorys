﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour
{
    [SerializeField] private string _name = string.Empty; 

    [Header("Setting")]
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _scaleTime;
    [SerializeField] private float _disapearTime;

    private SpriteRenderer _spriteRenderer;

    public string Name 
    {
        get 
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }
    public Sprite ItemSprite => _spriteRenderer.sprite; 

    public System.Action<Item> OnFind;

    public void Initialized()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void Awake()
    {
        Initialized();
    }
    void OnMouseDown()
    {
        _coroutine = StartCoroutine(Scaling());
        

    }
    private Coroutine _coroutine = null;
    private IEnumerator Scaling()
    {
        Vector3 startScale = transform.localScale;
        float currentMultiplier = 1f;
        float speed = (_scaleMultiplier - currentMultiplier) / _scaleTime;

        while(currentMultiplier < _scaleMultiplier)
        {
            Vector3 currentScale = startScale * currentMultiplier;
            currentMultiplier += speed * Time.deltaTime;
            transform.localScale = currentScale;
            

            yield return null;
        }
        yield return new WaitForSeconds(1f);

        float currentAlpha = 1f;
        float alphaSpeed = 1f / _disapearTime;

        while(currentAlpha > 0)
        {
            Color color = _spriteRenderer.color;
            color.a = currentAlpha;
            currentAlpha -= alphaSpeed * Time.deltaTime;
            _spriteRenderer.color = color;

            yield return null;
        }
        DisableObject();
    }
    private void DisableObject()
    {
        gameObject.SetActive(false);
        OnFind?.Invoke(this); 
    }
}
