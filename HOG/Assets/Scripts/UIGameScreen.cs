﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIGameScreen : MonoBehaviour
{
    [SerializeField] private RectTransform _content = null;
    [SerializeField] private GameObject _itemPrefab = null;
    

    private List<UIItemController> _uiItems = new List<UIItemController>();

    Dictionary<string, ItemData> _items;

    public void Initialize(Level level)
    {
        level.OnItemList += UpdateItemList;

        _items = level.GetItemsData();

        foreach (string key in _items.Keys)
        {
            GameObject newItem = Instantiate(_itemPrefab, _content);
            newItem.GetComponent<UIItemController>().Initialize(_items[key].ItemSprite, _items[key].Amount, key);
            _uiItems.Add(newItem.GetComponent<UIItemController>());
        }  
    }

    private void UpdateItemList(string name)
    {
        foreach (var item in _uiItems)
        {
            if (item.Name == name)
            {
                item.Decrease();
            }
        }
        
        
    }
    
}
