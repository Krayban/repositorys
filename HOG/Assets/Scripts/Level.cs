﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private List<Item> _gameItems = new List<Item>();
    private Dictionary<string, ItemData> _itemData = new Dictionary<string, ItemData>(); 

    public delegate void OnComplete();
    public OnComplete OnLevelCompleted;

    public System.Action<string> OnItemList;

    public void Initialized()
    {
        _gameItems = new List<Item>(GetComponentsInChildren<Item>());

        foreach (var i in _gameItems)
        {
            i.Initialized();
            i.OnFind += OnFindAction;

            if (!_itemData.ContainsKey(i.Name)) 
            {
                _itemData.Add(i.Name, new ItemData(i.ItemSprite, 1));
            }
            else
            {
                _itemData[i.Name].Amount++;   
            }
        }
    }
    private void Start()
    {
        Initialized();
    }

    public Dictionary<string, ItemData> GetItemsData() 
    {
        return _itemData;
    }
    private void OnFindAction(Item item)
    {
        _gameItems.Remove(item);
        item.OnFind -= OnFindAction;
        OnItemList?.Invoke(item.Name);

        if (_gameItems.Count == 0)
        {
            OnLevelCompleted?.Invoke();
        }
    }

}
