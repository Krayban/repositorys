﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemController : MonoBehaviour
{
    [SerializeField] private Text _countText = null;
    [SerializeField] private Image _image = null;

    public string Name;

    private int _count = 0;

    public void Initialize(Sprite sprite, int count, string Name)
    {
        _image.sprite = sprite;
        _count = count;
        _countText.text = count.ToString();
        this.Name = Name;
    }

    public void Decrease()
    {
        _count--;
        if (_count <= 0)
        {
            
            gameObject.SetActive(false);
            
        }
        else
        {
            _countText.text = _count.ToString();
        }
    }
}
