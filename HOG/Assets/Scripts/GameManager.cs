﻿using GameAnalyticsSDK.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Level> _levels;
    [SerializeField] private GameObject _mainScreen = null;
    [SerializeField] private UIGameScreen _gameScreen = null;
    [SerializeField] private GameObject _winScreen = null;
    [SerializeField] private float time;
    [SerializeField] private Text timerText;
    [SerializeField] private GameObject _FailScreen;

    private float _time = 0f;

    private float _height;
    private float _width;

    [Header("References")]
    [SerializeField] private AdsManager _adsManager = null;

    private GameObject _levelObject = null;

    private void Awake()
    {
        _adsManager.Initialize();
    }

    private void Start()
    {
        GameAnalyticsSDK.GameAnalytics.Initialize();
        _mainScreen.SetActive(true);
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent (GameAnalyticsSDK.GAProgressionStatus.Start, "Level");
        GameAnalyticsSDK.GameAnalytics.NewDesignEvent("hello", 1);
        _height = Camera.main.orthographicSize;
        _width = Camera.main.orthographicSize * Camera.main.aspect;
        

    }

    private void Update()
    {
        Camera.main.orthographicSize = _width / Camera.main.aspect;

    }

    public void StartGame()
    {
        _mainScreen.SetActive(false);
        _gameScreen.gameObject.SetActive(true);
        
        int index = Random.Range(0, _levels.Count);
        InstaniateLevel(index);

        _time = time;
        StartCoroutine(StartTimer());

        
    }
    private void InstaniateLevel(int index)
    {
        if (_levelObject != null)
        {
            Destroy(_levelObject);
            _levelObject = null;
        }
        if (index >= _levels.Count)
        {
            return;
        }
        _levelObject = Instantiate(_levels[index].gameObject, transform);
        Level level = _levelObject.GetComponent<Level>();
        level.Initialized();
        _gameScreen.Initialize(level);
        level.OnLevelCompleted += OnLevelComplete;
    }
    private void OnLevelComplete()
    {
        _gameScreen.gameObject.SetActive(false);
        _winScreen.SetActive(true);
        _levelObject.GetComponent<Level>().OnLevelCompleted -= OnLevelComplete;
        _levelObject.SetActive(false);
        _adsManager.ShowInterstitial();
        StopAllCoroutines();


        
    }
    public void ExitLevel()
    {
        _winScreen.SetActive(false);
        _mainScreen.SetActive(true);
    }

    private IEnumerator StartTimer()
    {
        while (_time > 0)
        {
            _time -= Time.deltaTime;
            UpdateTimeText();
            yield return null;

        }
        if (_time == 0)
        {
            _gameScreen.gameObject.SetActive(false);
            _levelObject.GetComponent<Level>().OnLevelCompleted -= OnLevelComplete;
            _levelObject.SetActive(false);
            _FailScreen.gameObject.SetActive(true);
            StopAllCoroutines();
        }
    }

    private void UpdateTimeText()
    {
        if (_time < 0)
            _time = 0;

        float minutes = Mathf.FloorToInt(_time / 60);
        float seconds = Mathf.FloorToInt(_time % 60);
        timerText.text = string.Format("{0:00} : {1:00}", minutes, seconds);
    }

   

}
