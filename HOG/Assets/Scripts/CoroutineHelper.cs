﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineHelper 
{
    public static void DelayCall(MonoBehaviour obj, System.Action action, float delay)
    {

    }
    private static IEnumerator Delay(float time, System.Action action)
    {
        yield return new WaitForSeconds(time);

        action?.Invoke();
    }
}
