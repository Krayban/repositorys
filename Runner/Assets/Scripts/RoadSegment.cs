﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSegment : MonoBehaviour
{
    [SerializeField] private Transform _nextPoint = null;

    public Vector3 NextPosition => _nextPoint.position; 
}
