﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField] private Animator _animator = null;

    private int _runID = Animator.StringToHash("isRun");

    public void SetTrigger(string name)
    {
        _animator.SetTrigger(name);
    }
}
