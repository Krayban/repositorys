﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private float _speed = 0f;
    [SerializeField] private InputHandler _inputHandler = null;
    [SerializeField] private float _roadWidth = 1f;
    [SerializeField] private AnimationController _animation = null;

    public Text text;
    public static int _coin = 0;

    public System.Action OnTriggerWin;
    public System.Action OnTriggerFall;

    private void Start()
    {
        _animation.SetTrigger("isIdle");
    }

    private void Update()
    { 
        Move();
    }

    private void Move()
    {
        Vector3 horizontalOffset = _inputHandler.OffsetX * _roadWidth * Vector3.right;

        float resultOffset = transform.position.x + horizontalOffset.x;
        if(Mathf.Abs(resultOffset) > _roadWidth / 2f)
        {
            horizontalOffset.x = 0f;
        }
            Vector3 forwardOffset = Vector3.forward * Time.deltaTime * _speed;
            transform.Translate(forwardOffset + horizontalOffset);

        _animation.SetTrigger("isRun");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Cube")
        {
            
            _speed = 0;
            _animation.SetTrigger("isDead");

            OnTriggerFall?.Invoke();
        }

        if (other.gameObject.tag == "Finish")
        {
            _speed = 0;
            _animation.SetTrigger("isDance");

            OnTriggerWin?.Invoke();
        }
    }
}
