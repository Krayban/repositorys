﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    private GameData gameData = new GameData();

    private void Start()
    {
        string json = JsonUtility.ToJson(gameData, true);
        Debug.Log(json);
    }
}
[System.Serializable]
public class GameData
{
    public int Health;
    public int Coin;
    public int Exp;


    public GameData()
    {
        Health = 0;
        Coin = 0;
        Exp = 0;
    }
}

