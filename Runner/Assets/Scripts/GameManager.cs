﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _startScreen;
    [SerializeField] private GameObject _gameScreen;
    [SerializeField] private GameObject _winScreen;
    [SerializeField] private GameObject _fallScreen;
    [SerializeField] private List<Level> _levels;

    private GameObject _levelObject = null;
    

    private void Start()
    {
        _startScreen.gameObject.SetActive(true);
    }

    public void StartGame()
    {
        _gameScreen.SetActive(true);
        _startScreen.SetActive(false);
        int index = Random.Range(0, _levels.Count);
        InstaniateLevel(index);
        _fallScreen.SetActive(false);
        _winScreen.SetActive(false);
    }

    public void OnLevelCompleate()
    {
        _winScreen.SetActive(true);
    }
    public void FallLevel()
    {
        _fallScreen.SetActive(true);
    }

    public void ExitLevel()
    {
        _winScreen.SetActive(false);
        _startScreen.SetActive(true);
        _fallScreen.SetActive(false);
    }
    private void InstaniateLevel(int index)
    {
        if (_levelObject != null)
        {
            Destroy(_levelObject);
            _levelObject = null;
        }
        if (index >= _levels.Count)
        {
            return;
        }
        _levelObject = Instantiate(_levels[index].gameObject, transform);
        Level level = _levelObject.GetComponent<Level>();
        level.GenerateLevel();
        OnTriggerAction(level.Player);
    }

    public void OnTriggerAction(Player player)
    {
        player.OnTriggerWin += OnLevelCompleate;
        player.OnTriggerFall += FallLevel;
    }

   
    

}
