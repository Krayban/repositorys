﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    private float _offset;
    private Vector3 _startPosition;
    public float OffsetX => _offset;
   
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startPosition = Input.mousePosition;

        }

        if(Input.GetMouseButton(0))
        {
            _offset = -(_startPosition - Input.mousePosition).x;
            _offset /= Screen.width;
            _startPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _offset = 0;
        }

    }

    


}
