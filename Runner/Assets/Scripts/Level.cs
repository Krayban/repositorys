﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] private GameObject _roadPrefab = null;
    [SerializeField] private Transform _startPoint = null;
    [SerializeField] private int _startSegmentCount = 10;
    [SerializeField] private GameObject _finishPrefab = null;
    [SerializeField] private GameObject _playerPrefab = null;
    [SerializeField] private GameObject _damagePrefab = null;

    [SerializeField] private float _minDamageOffset = 3f;
    [SerializeField] private float _maxDamageOffset = 5f;
    [SerializeField] private float _roadPartLength = 5f;
    [SerializeField] private float _roadPartWidth = 6f;
    
    private Vector3 _playerLocalPosition = Vector3.zero;
    private Player _player = null;
    public Player Player => _player;

    private Vector3 _lastPoint ;

    public Vector3 _size;
    public Vector3 _center;

    public void Start()
    {
        
    }

    public void GenerateLevel()
    {
        Clear();
        GenerateRoad();
        GenerateDamages();
        GeneratePlayer();
    }

    private void Clear()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        _player = null;
    }

    private void GenerateRoad()
    {
        Vector3 roadLocalPosition = Vector3.zero;

        for (int i = 0; i < _startSegmentCount; i++)
        {
            GameObject roadPart = Instantiate(_roadPrefab, transform);
            roadPart.transform.localPosition = roadLocalPosition;

            roadLocalPosition.z += _roadPartLength;
        }

        GameObject finishRoad = Instantiate(_finishPrefab, transform);
        finishRoad.transform.localPosition = roadLocalPosition;
    }

    private void GenerateDamages()
    {
        float fullLength = _startSegmentCount * _roadPartLength;
        float currentLength = _roadPartLength * 2;
        float damageOffsettX = _roadPartWidth / 3f;
        float startPosX = -_roadPartWidth / 2f;

        while (currentLength < fullLength)
        {
            float zOffset = _minDamageOffset + Random.Range(_minDamageOffset, _maxDamageOffset);
            currentLength += zOffset;
            currentLength = Mathf.Clamp(currentLength, 0f, fullLength);

            int damagePosition = Random.Range(0, 3);
            float damagePosX = startPosX + damageOffsettX * damagePosition;

            GameObject damage = Instantiate(_damagePrefab, transform);
            Vector3 localPosition = Vector3.zero;
            localPosition.x = damagePosX;
            localPosition.z = currentLength;

            damage.transform.localPosition = localPosition;
        }
    }
    
    private void GeneratePlayer()
    {
        GameObject player = Instantiate(_playerPrefab, transform);
        player.transform.localPosition = new Vector3(0f, 1.8f, _roadPartLength / 2f);

        _player = player.GetComponent<Player>();
    }
}
